# ​New Object From Selected Geometry

### Overview

This add-on allows you to create a new object using the selected geometry when in edit-mode. It can be used to create an object using geometry from different objects (maybe to fill gaps between objects, to trace lines, etc.).

The new object will be created with the origin at the cursor, but the geometry position is maintained as it is. So the objects and cursor should be positioned as required before using the add-on. It's necessary to apply the modifiers before using the reference objects.\
The new object will be created in the active collection with the name: _objectFromVertices_, or _objectFromEdges_, or _objectFromFaces_ depending on the mode of operation used. It can be easily renamed on the Outliner editor.\

Once installed and enabled the add-on is available in the Mesh menu in Edit Mode (the shortcut is Alt-O).

### Installing

#### To install the extension on Blender 4.2 and above:

You can look on https://extensions.blender.org/

Otherwise to install as an Extension:

1- Download the _New object from selected geometry.zip_ file to a folder of your choice.\
2- Open the Preferences window (Edit/Preferences) in Blender (4.2.0 or above) and select the _Get Extensions_ tab.\
3- Select _Install from disk_ clicking on the down arrow on the top right of the window (next to the repositories drop-down).\
4- Navigate to the folder of the download and select the _NewObjectFromSelectedGeometry.zip_ file.\
5- Enable the add-on once it is installed (or check the option _Enable on Install_).

#### To install the add-on on previous versions of Blender (from 3.5.0):

1- Download the _NewObjectFromSelectedGeometry-forBlender3-5.py_ file to a folder of your choice.\
2- Open the Preferences window (Edit/Preferences) in Blender (3.5.0 or above) and select the Add-ons tab.\
3- Select _Install…_ on the top right of the window.\
4- Navigate to the folder where the file was downloaded and select it.\
5- Enable the add-on once it is installed.

### Usage

To use the add-on follow these steps:

1- Position the Cursor and objects containing the geometry needed for the new object in the desired position.\
2- Select the objects. Note that the modifiers need to be applied before using the object. Use copies of objects if you don't want to change the original objects...\
3- Enter edit mode and select the vertices/edges or faces to be added to the new object.\
4- Press Alt-O or use the Mesh menu and click on _New object from selected geometry_.\
5- Adjust the options to get a better result (depending on what you need on your new object).\
6- The add-on has 3 modes of operation: "Create object from faces", "Create object from edges", "Create object from vertices". What they do should be self explanatory.\
&nbsp;&nbsp;&nbsp; To use the mode "Create object from faces" at least 1 face needs to be selected.\
&nbsp;&nbsp;&nbsp; To use the mode "Create object from edges" at least 2 edges need to be selected.\
&nbsp;&nbsp;&nbsp; To use the mode "Create object from vertices" at least 3 vertices need to be selected (or 4 vertices if the Even-Odd option is enabled).

**Notes:**

*Create object from faces:*\
● The order of selection of the faces is not taken into consideration.\
● The normals of the faces will be the same as in the original objects, so they may need to be adjusted (depending on the face orientation on the original objects).\
● From version 1.0.2 an option has been added to flip the face normals.\
● From version 1.0.2 an option has been added to try to add edges to help filling of holes in the mesh.\
● From version 1.0.2 an option has been added to try to fill the holes in the mesh. The filling works best when there are small gaps. If the faces overlap or if there are big gaps, the result will not be great. Maybe in future versions this can be improved.


*Create object from edges:*\
● This mode will work when edges or faces are selected (it may not work if only random vertices are selected).\
● The direction of the edges will be the same as in the original objects (the 1st and 2nd vertices are the in the same position as in the original).\
● The order of selection of the edges is not taken into consideration (because adjacent edges may be in opposite directions).

*Create object from vertices:*\
● If edges/faces are selected, the order of the vertices will depend on their order in the edges/faces (so the new edges may look messy and overlap each other).\
● By default the add-on will draw edges between the vertices. This can be disabled in the options.\
● The edges are drawn between the vertices in the order of selection. The order of selection only works when each vertex is selected individually by the user (blender does not save a selection history when vertices are selected using a tool [box selection, etc.]).\
● When using more than 1 object, the selection list is created adding all the selected vertices of each object (so the order of selection is "per object", in the **inverse** order of selection of the objects - first the *active object*, etc.). If the order of the vertices is important (for example because the new object will be converted into a curve), then the quickest way would be to select all the required vertices from the objects as quickly as possible using the selection tools, then once the new object is created use the new object to select the vertices in the order required and use the add-on again.

### Options

The add-on has options available after it has been used:

- _Mode_: This option is used to change the mode of operation of the add-on.

- _Draw edges_: This option affects only the "Create object from vertices" mode of operation. This is enabled by default and will draw the edges between the vertices. If the edges are not required or they are not "workable", they can be removed by unchecking the tick-box.

- _Even-Odd_: This option affects only the "Create object from vertices" mode of operation. If this option is enabled the edges will be drawn between even vertices and between odd vertices (according to the order of selection). So the 1st vertex will be joined to the 3rd, then the 5th, etc.; the 2nd vertex will be joined to the 4th, then the 6th, etc. This option might be useful if the edges drawn by default are "unworkable", and drawing them in a different order might give a more "workable" result.

- _Join ends_: This option affects only the "Create object from vertices" mode of operation. This option is enabled by default and will draw an edge between the 1st and last vertices. It can be removed by unchecking the tick-box.

- _Normals inside_: This option affects only the "Create object from faces" mode of operation. This option is enabled by default and will flip the normals of the faces (so the inside is towards the space being filled, instead of the inside of the original object(s)). If the result is not as expected, it can be disabled and the mesh can be adjusted manually.

- _Try adding edges_: This option affects only the "Create object from faces" mode of operation. This option is enabled by default and will draw edges between vertices that are connected to only 2 edges. This adds geometry that can help filling holes in the mesh. If the result is not as expected, it can be disabled and the mesh can be adjusted manually.

- _Try filling holes_: This option affects only the "Create object from faces" mode of operation. This option is enabled by default and will use the built-in Mesh/Clean Up/Fill Holes operation to try to fill holes in the mesh. If the result is not as expected, it **should** be disabled and the mesh can be adjusted manually.
