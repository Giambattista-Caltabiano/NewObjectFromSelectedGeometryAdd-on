# New Object From Selected Geometry Blender Add-on

This add-on was originally created using Bleder 3.5.0 and tested on it.

The latest version was updated and tested in Blender 4.2.0

Version 1.0.1 packed as an Extension for Blender 4.2.0 has the translation into Italian and Spanish.

See the Documentation for the details on how to install and use the Add-on.
