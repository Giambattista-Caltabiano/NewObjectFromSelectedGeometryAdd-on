#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License v3.0 or later
#  https://www.gnu.org/licenses/gpl-3.0-standalone.html
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
bl_info = {
    "name": "New Object From Selected Geometry",
    "description": "Creates a new object from the selected geometry in Edit mode",
    "author": "Giambattista Caltabiano",
    "version": (1, 0, 2),
    "blender": (3, 5, 0),
    "location": "View3D > Mesh",
    "warning": "",
    "doc_url": "https://projects.blender.org/Giambattista-Caltabiano/NewObjectFromSelectedGeometryAdd-on.git",
    "tracker_url": "https://projects.blender.org/Giambattista-Caltabiano/NewObjectFromSelectedGeometryAdd-on.git/issues",
    "category": "Mesh",
}

import bpy
import bmesh

def vertDistance(v1, v2):
    import math
    dx, dy, dz = v1.co - v2.co
    return math.sqrt(dx ** 2 + dy ** 2 + dz ** 2 )

class ObjectFromGeometry(bpy.types.Operator):
    """Create an object from selected geometry"""      # Use this as a tooltip for menu items and buttons.
    bl_idname = "object.new_from_geometry"        # Unique identifier for buttons and menu items to reference.
    bl_label = "New object from selected geometry"         # Display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}  # Enable undo for the operator.

    modeOptions: bpy.props.EnumProperty(
        items=[
            ("useFaces", "Object from Faces", "Create object from faces", "", 0),
            ("useEdges", "Object from Edges", "Create object from edges", "", 1),
            ("useVerts", "Object from Vertices", "Create object from vertices", "", 2),
               ],
        name = "Mode",
        default = "useFaces",
        )

    drawEdges: bpy.props.BoolProperty(name="Draw edges", description="If checked, edges are drawn between the vertices", default=True)
    evenOdd: bpy.props.BoolProperty(name="Even-Odd", description="If checked, the edges are created between even vertices and between odd vertices (the first is connected to the third, the second is connected to the fourth, etc.). Need to select at least 4 vertices", default=False)
    joinEnds: bpy.props.BoolProperty(name='Join ends', description='If checked, an edge is added between the first and last selected vertices', default=True)

    flipNormals: bpy.props.BoolProperty(name='Normals inside', description='If checked, it flips the face normals', default=True)

    tryAddEdges: bpy.props.BoolProperty(name='Try adding edges', description='If checked, it tries to add edges between vertices connected to less than 3 edges',default=True)

    tryFillHoles: bpy.props.BoolProperty(name='Try filling holes', description='If checked, it tries to fill holes in the mesh', default=True)

    def execute(self, context):     # execute() is called when running the operator.
        bpy.ops.object.mode_set(mode='OBJECT') # needed to update the vertex selection
        activeObj = context.view_layer.objects.active
        meshCopy = activeObj.data.copy()  # keeping the copy of the active object data for the new object
        selectedVerts = []
        selectedEdges = []
        selectedFaces = []
        bm = bmesh.new()
        selectedObjs = bpy.context.view_layer.objects.selected
        originalObjs = [] # cannot use selectedObjs because it is being changed with the selection
        copyObjs = []  # copies/duplicates are needed to change the origins without affecting the originals
        for o in selectedObjs:
            originalObjs.append(o)
        for o in originalObjs:
            bpy.ops.object.select_all(action='DESELECT')
            o.select_set(True)
            bpy.ops.object.duplicate()  # need to duplicate the mesh to stop affecting the originals
            o.select_set(False)
            copyObjs.append(bpy.context.view_layer.objects.selected[0])
        bpy.ops.object.select_all(action='DESELECT')
        for o in copyObjs:
            o.select_set(True)
            bpy.ops.object.origin_set(type='ORIGIN_CURSOR') # using to keep the geometry position relative to the cursor
            bpy.ops.object.transform_apply(location = True, scale = True, rotation = True)
        for o in copyObjs:
            bm.from_mesh(o.data)
            bm.verts.ensure_lookup_table()
            for vert in bm.verts:         # remove non-selected vertices
                if not vert.select:
                    bm.verts.remove(vert)
            if len(bm.select_history) == len(bm.verts): # vertices selected individually, using order of selection
                selectedVerts.extend(bm.select_history)
            else: selectedVerts.extend(bm.verts)
            for edge in bm.edges:
                if not edge.select:
                    bm.edges.remove(edge)
            selectedEdges.extend(bm.edges)
            for face in bm.faces:
                if not face.select:
                    bm.faces.remove(face)
            selectedFaces.extend(bm.faces)
        selectedVerts = list(dict.fromkeys(selectedVerts)) # remove duplicates
        if len(selectedVerts) < 3:
            self.report({'ERROR'}, "You need to select at least 3 vertices, or 2 edges, or 1 face")
            for o in copyObjs:  # removing copies
                bpy.data.objects.remove(o, do_unlink=True)
            for o in originalObjs:
                o.select_set(True)
            bpy.context.view_layer.objects.active = activeObj
            bpy.ops.object.mode_set(mode='EDIT')
            bm.free()
            return {'CANCELLED'}
        done = False
        if self.modeOptions == "useFaces":
            if len(selectedFaces) > 0:
                bmFaces = bmesh.new()
                for f in selectedFaces:
                    vertsList = []
                    for v in f.verts:
                        bmFaces.verts.new(v.co)
                        bmFaces.verts.ensure_lookup_table()
                        vertsList.append(bmFaces.verts[len(bmFaces.verts) - 1])
                    bmFaces.faces.new(vertsList)
                bmesh.ops.remove_doubles(bmFaces,verts=bmFaces.verts,dist=0.001)
                bmFaces.faces.ensure_lookup_table()
                for f in bmFaces.faces:
                    if self.flipNormals == True:
                        f.normal_flip()
                    f.select_set(True)
                if self.tryAddEdges == True: # made function to make it easier to read
                    self.addEdgesToVerticesWithLessThan3linkedEdges(bmFaces) # adding the edges gives the fill operation more geometry to work with
                    # filling is done with bpy.ops at the end
                bmFaces.to_mesh(meshCopy)
                ob = bpy.data.objects.new('objectFromFaces',meshCopy)
                ob.location = context.scene.cursor.location
                context.collection.objects.link(ob)
                bpy.ops.object.select_all(action='DESELECT')
                ob.select_set(True)
                bpy.context.view_layer.objects.active = ob
                done = True
            else:
                self.report({'ERROR'}, "No face selected for the Object from Faces mode, switching to the Object from Vertices mode")
                self.modeOptions = "useVerts"
        if self.modeOptions == "useEdges":
            if len(selectedEdges) > 1:
                bmEdges = bmesh.new()
                for e in selectedEdges:
                    vertsList = []
                    for v in e.verts:
                        bmEdges.verts.new(v.co)
                        bmEdges.verts.ensure_lookup_table()
                        vertsList.append(bmEdges.verts[len(bmEdges.verts) - 1])
                    bmEdges.edges.new(vertsList)
                bmesh.ops.remove_doubles(bmEdges,verts=bmEdges.verts,dist=0.001)
                bmEdges.edges.ensure_lookup_table()
                for e in bmEdges.edges:
                    e.select_set(True)
                bmEdges.to_mesh(meshCopy)
                ob = bpy.data.objects.new('objectFromEdges',meshCopy)
                ob.location = context.scene.cursor.location
                context.collection.objects.link(ob)
                bpy.ops.object.select_all(action='DESELECT')
                ob.select_set(True)
                bpy.context.view_layer.objects.active = ob
                done = True
            else:
                self.report({'ERROR'}, "You need to select at least 2 edges for the Object from Edges mode, switching to the Object from Vertices mode")
                self.modeOptions = "useVerts"
        if self.modeOptions == "useVerts" and done == False:
            bmVerts = bmesh.new()
            for v in selectedVerts:
                bmVerts.verts.new(v.co)
            for v in bmVerts.verts:
                v.select_set(True)
            bmVerts.verts.ensure_lookup_table()
            if self.drawEdges == True:
                numVertices = len(bmVerts.verts)
                if self.evenOdd == False:
                    if self.joinEnds == True:
                        for i in range(numVertices):
                            bmVerts.edges.new((bmVerts.verts[i-1],bmVerts.verts[i]))
                    else:
                        for i in range(numVertices-1):
                            bmVerts.edges.new((bmVerts.verts[i],bmVerts.verts[i+1]))
                else:
                    if numVertices < 4:
                        self.report({'ERROR'}, "You need to select at least 4 vertices if the Even-Odd option is enabled")
                        self.evenOdd = False
                        for o in copyObjs:  # removing copies
                            bpy.data.objects.remove(o, do_unlink=True)
                        for o in originalObjs:
                            o.select_set(True)
                        bpy.context.view_layer.objects.active = activeObj
                        bpy.ops.object.mode_set(mode='EDIT')
                        bmVerts.free()
                        bm.free()
                        return {'CANCELLED'}
                    for i in range(numVertices-2):
                        bmVerts.edges.new((bmVerts.verts[i],bmVerts.verts[i+2]))
                    if self.joinEnds == True:
                        bmVerts.edges.new((bmVerts.verts[numVertices-1],bmVerts.verts[0]))
                for e in bmVerts.edges:
                    e.select_set(True)
            bmVerts.to_mesh(meshCopy)
            ob = bpy.data.objects.new('objectFromVertices',meshCopy)
            ob.location = context.scene.cursor.location
            context.collection.objects.link(ob)
            bpy.ops.object.select_all(action='DESELECT')
            ob.select_set(True)
            bpy.context.view_layer.objects.active = ob
        if self.tryFillHoles == True and self.modeOptions == "useFaces":
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.fill_holes(sides=0)
            bpy.ops.mesh.delete_loose(use_verts=True, use_edges=True, use_faces=False)
            bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        for o in copyObjs:  # removing copies
            bpy.data.objects.remove(o, do_unlink=True)
        bm.free()
        return {'FINISHED'}

    # def invoke(self, context, event):
    #     wm = context.window_manager
    #     return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "modeOptions")
        col = layout.column()
        sub = col.column()
        if self.modeOptions == "useFaces":
            sub.prop(self,"flipNormals")
            sub.prop(self,"tryAddEdges")
            sub.prop(self,"tryFillHoles")
        if self.modeOptions == "useVerts":
            sub.prop(self,"drawEdges")
            sub.prop(self,"evenOdd")
            sub.prop(self,"joinEnds")
        layout.row().separator()

    def addEdgesToVerticesWithLessThan3linkedEdges(self, bm):
        bmesh.ops.remove_doubles(bm,verts=bm.verts,dist=0.001)
        bm.verts.ensure_lookup_table()
        repeatFillOp = True
        while (repeatFillOp == True):
            repeatFillOp = False
            lowerGap = {'v0': -1, 'v1': -1, 'd': 0 } # will add an edge between the verts with the lowest gap
            bm.verts.ensure_lookup_table()
            for v in bm.verts:
                sameGeoVerts= []  # same geometry vertices will be excluded
                if len(v.link_edges) < 3:
                    sameGeoVerts.append(v.index)
                    for e in v.link_edges: # verts on linked edges
                        sameGeoVerts.append(e.other_vert(v).index)
                    for f in v.link_faces:
                        for ver in f.verts:
                            if v.index != ver.index:
                                sameGeoVerts.append(ver.index)
                    sameGeoVerts = list(dict.fromkeys(sameGeoVerts)) # remove duplicates
                    closestVert = { 'v': -1, 'd': 0 }
                    for ver in bm.verts:
                        if ver.index not in sameGeoVerts:
                            if len(ver.link_edges) < 3:
                                d = vertDistance(ver, bm.verts[v.index])
                                if closestVert['v'] == -1:
                                    closestVert['v'] = ver.index
                                    closestVert['d'] = d
                                else:
                                    if closestVert['d'] > d:
                                        closestVert['v'] = ver.index
                                        closestVert['d'] = d
                    if closestVert['v'] != -1:
                        if lowerGap['v0'] == -1:
                            lowerGap["v0"] = v.index
                            lowerGap["v1"] = closestVert['v']
                            lowerGap["d"] = closestVert['d']
                        else:
                            if closestVert['d'] < lowerGap["d"]:
                                lowerGap["v0"] = v.index
                                lowerGap["v1"] = closestVert['v']
                                lowerGap["d"] = closestVert['d']
            if lowerGap['v0'] != -1:
                bm.edges.new((bm.verts[lowerGap["v0"]],bm.verts[lowerGap["v1"]]))
                bm.faces.ensure_lookup_table()
                repeatFillOp = True  # will repeat until all vertices have at least 3 link_edges
        bm.verts.ensure_lookup_table()
        bm.edges.ensure_lookup_table()
        bm.faces.ensure_lookup_table()

def menu_func(self, context):
    self.layout.operator(ObjectFromGeometry.bl_idname)

# store keymaps here to access after registration
addon_keymaps = []

def register():
    bpy.utils.register_class(ObjectFromGeometry)
    bpy.types.VIEW3D_MT_edit_mesh.append(menu_func)  # Adds the new operator to an existing menu.

    # handle the keymap
    wm = bpy.context.window_manager
    # Note that in background mode (no GUI available), keyconfigs are not available either,
    # so we have to check this to avoid nasty errors in background case.
    kc = wm.keyconfigs.addon
    if kc:
        km = wm.keyconfigs.addon.keymaps.new(name='Mesh', space_type='EMPTY')
        kmi = km.keymap_items.new(ObjectFromGeometry.bl_idname, 'O', 'PRESS', alt=True)
        addon_keymaps.append((km, kmi))

    #bpy.app.translations.register(__name__, translations.translations_dict)

def unregister():
    # Note: when unregistering, it's usually good practice to do it in reverse order you registered.

    #bpy.app.translations.unregister(__name__)

    # Can avoid strange issues like keymap still referring to operators already unregistered...
    # handle the keymap
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    bpy.utils.unregister_class(ObjectFromGeometry)
    bpy.types.VIEW3D_MT_edit_mesh.remove(menu_func)


# This allows you to run the script directly from Blender's Text editor
# to test the add-on without having to install it.
if __name__ == "__main__":
    register()